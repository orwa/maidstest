import { HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class CachingService {
  cachekey: string = 'maids_test_keys';
  // determine max size of cache in KB
  MaxSize: number = 6;
  storeInCache(url: string, data: HttpResponse<any> , timeToExpire:number) {
    var cachingData: CacheItm[] = [];
    cachingData = JSON.parse(localStorage.getItem(this.cachekey));
    if (!cachingData) {
      cachingData = [];
    }
    const expires = new Date();
    expires.setSeconds(expires.getSeconds() + timeToExpire);
    cachingData.push(new CacheItm(url, data, 1,expires));
    localStorage.setItem('tmp-cache', JSON.stringify(cachingData));
    if (this.getChacheSize('tmp-cache') < this.MaxSize) {
      localStorage.removeItem('tmp-cache');
      localStorage.setItem(this.cachekey, JSON.stringify(cachingData));
    } else {
      localStorage.removeItem('tmp-cache');
      cachingData = JSON.parse(localStorage.getItem(this.cachekey));
      cachingData[0] = new CacheItm(url, data, 1,expires);
      localStorage.setItem(this.cachekey, JSON.stringify(cachingData));
    }
  }

  getFromCache(url: string) : HttpResponse<any> {
    var cachingData: CacheItm[];
    cachingData = JSON.parse(localStorage.getItem(this.cachekey));
    if (cachingData) {
      let cacheItem: CacheItm = cachingData.filter(
          (cache) => cache.url === url)[0];
      if (cacheItem) {
        const expires = new Date(cacheItem.expires);
        const now = new Date();
        if (expires && expires.getTime() < now.getTime()) {
           this.deleteItemByUrl(url);
           return null;
        }
        cacheItem.frequency = cacheItem.frequency + 1;
        var itemsSorted = cachingData.sort((a, b) => a.frequency - b.frequency);
        localStorage.setItem(this.cachekey, JSON.stringify(itemsSorted));
        return cacheItem.data;
      }
    }
    return null;
  }

  deleteItemByUrl(url: string){
    var cachingData: CacheItm[];
    cachingData = JSON.parse(localStorage.getItem(this.cachekey));
    if (cachingData) {
      let cacheItem: CacheItm = cachingData.filter(
                      (cache) => cache.url === url)[0];
      if (cacheItem) {
        let indexToDelete = cachingData.findIndex((item)=>item.url===cacheItem.url);
        if(indexToDelete>-1){
          cachingData.splice(indexToDelete,1);
        }
        var itemsSorted = cachingData.sort((a, b) => a.frequency - b.frequency);
        localStorage.setItem(this.cachekey, JSON.stringify(itemsSorted));
        return cacheItem.data;
      }
    }
  }

 private getChacheSize(cachekey: string) {
    return (
      (localStorage[cachekey].length * 2 + this.cachekey.length * 2) / 1024
    );
  }
}

class CacheItm {
  constructor(
    public url: string,
    public data: HttpResponse<any>,
    public frequency: number,
    public expires: Date
  ) {}
}
