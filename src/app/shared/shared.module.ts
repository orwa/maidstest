import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

//service
import {CachingService} from './service/caching.service'
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { CacheInterceptor } from '../interceptors/cache.interceptor';
@NgModule({
  declarations: [],
  imports: [CommonModule,FormsModule,ReactiveFormsModule,RouterModule],
  providers: [CachingService, { provide: HTTP_INTERCEPTORS, useClass: CacheInterceptor, multi: true }],
  exports:[CommonModule,FormsModule,ReactiveFormsModule,RouterModule],
})
export class SharedModule {}
