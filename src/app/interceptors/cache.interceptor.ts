import { Injectable } from '@angular/core';
import { HttpEvent, HttpResponse } from '@angular/common/http';
import { CachingService } from '../shared/service/caching.service';
import { tap, delay } from 'rxjs/operators';
import { Observable, of } from 'rxjs';

import {
  HttpRequest,
  HttpHandler,
  HttpInterceptor,
} from '@angular/common/http';

@Injectable()
export class CacheInterceptor implements HttpInterceptor {
  constructor(private cachingService: CachingService) {}

  intercept(req: HttpRequest<any>, next: HttpHandler) {
    if (req.headers.get('Use-Cache')) {
      const cachedResponse = this.cachingService.getFromCache(
        req.urlWithParams
      );
      if (cachedResponse) {
        return of(new HttpResponse({ body: cachedResponse.body }));
      } else {
        return this.sendRequest(req, next);
      }
    } else {
      this.sendRequest(req, next);
    }
  }

  sendRequest(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    return next.handle(req).pipe(
      // dealy to see animation because request is too fast , fell free to delete this line
      delay(1000),
      tap((response) => {
        if (response instanceof HttpResponse) {
          // expiration time should retutn from server and should retutrn condition to apply cache or not
          const timeToExpire = 10;
          this.cachingService.storeInCache(
            response.url,
            response,
            timeToExpire
          );
        }
      })
    );
  }
}
