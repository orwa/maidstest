import { NgModule } from '@angular/core';
import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import {SharedModule} from 'src/app/shared/shared.module'
import { MaterialModule } from 'src/app/material.module';
// componnent
import { UserListComponent } from './user-list/user-list.component';
import { UserDetailsComponent } from './user-details/user-details.component';
import { UsersRoutingModule } from './users-routing.module';
// service
import { UserApiService } from './service/userApi.service';
import { UserListResolverService } from './resolvers/user-list.resolver';
import { UserResolverService } from './resolvers/user.resolver';

@NgModule({
  declarations: [UserListComponent, UserDetailsComponent],
  imports: [SharedModule,HttpClientModule,MaterialModule,UsersRoutingModule],
  providers: [UserApiService,UserListResolverService,UserResolverService],
})
export class UsersModule {}
