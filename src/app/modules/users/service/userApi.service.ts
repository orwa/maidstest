import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CachingService } from 'src/app/shared/service/caching.service';

@Injectable()
export class UserApiService {
  endPoint = 'https://reqres.in/api/users';
  private headers = new HttpHeaders()
    .append('Content-Type', 'application/json')
    .append('Access-Control-Allow-Headers', 'Content-Type')
    .append('Access-Control-Allow-Origin', '*')
    .append('Access-Control-Allow-Methods', 'GET, POST, DELETE, PUT')
    .append('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

  constructor(private http: HttpClient,private cachingService:CachingService) {}

  fetchUsers(page,per_page,id): Observable<any> {
    var sendedHeaders = this.headers;
    sendedHeaders = sendedHeaders.append('Use-Cache', 'true');
    return this.http.get(this.endPoint, {
      headers: sendedHeaders,
      params: {
         page: page,
         per_page:per_page,
         id:id
        },
    });
  }

  fetchUser(id): Observable<any> {
    var sendedHeaders = this.headers;
    sendedHeaders = sendedHeaders.append('Use-Cache', 'true');
    return this.http.get(this.endPoint, {
      headers: sendedHeaders,
      params: {
         id:id
        },
    });
  }
}
