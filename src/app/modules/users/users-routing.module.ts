import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserListComponent } from './user-list/user-list.component';
import { UserDetailsComponent } from './user-details/user-details.component';
import { UserListResolverService } from './resolvers/user-list.resolver';
import { UserResolverService } from './resolvers/user.resolver';

const routes: Routes = [
  {
    path: '',
    component: UserListComponent,
    resolve: { users: UserListResolverService },
  },
  {
    path: ':id',
    component: UserDetailsComponent,
    resolve: { user: UserResolverService },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UsersRoutingModule {}
