import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { UserApiService } from '../service/userApi.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.sass']
})
export class UserListComponent implements OnInit,OnDestroy {
  // paginator
  @ViewChild("userPaginator") paginator: MatPaginator;
  pageSize = 6;
  pageSizeOptions = [4, 8,12];
  total;
  page;

  // search filter
  searchSubscribe : Subscription;
  seachValue="" ;

  users= [];
  isLoading: boolean = false;

  constructor(private userApiService:UserApiService,private route: ActivatedRoute,private router: Router,) { }


  ngOnInit(): void {
    let result =  this.route.snapshot.data.users;
    this.users =result.data;
    this.total = result.total;
    this.page = result.page;
    this.pageSize = result.per_page;
  }

  load(){
    this.isLoading = true;
    if(this.searchSubscribe){
      this.searchSubscribe.unsubscribe();
    }

    this.searchSubscribe =this.userApiService.fetchUsers(this.page,this.pageSize,this.seachValue).subscribe(
      (result)=>{
        if(Array.isArray(result.data)){
          this.users = result.data;
          this.total = result.total;
        }else{
          this.users =[];
          this.users.push(result.data);
          // this should return from api
          this.total = 1;
        }
        this.isLoading = false;
      },
      (error) => {
        if(error.status ===404){
          this.users =[];
          this.total = 0 ;
        }
        this.isLoading = false;
      },
    );

  }

  applyFilters(element) {
    this.seachValue = element.target.value;
    // if filter change should reset the paginator
    this.page = 1;
    this.paginator.pageIndex = 0;
    this.load();
  }

  onPaginateChange(event){
      this.pageSize = event.pageSize;
      this.page = event.pageIndex + 1;
      this.load();
  }

  navigateToUserPage(userID){
    this.isLoading = true;
    this.router.navigate(['users/' + userID]);
  }
  ngOnDestroy(): void {
    if(this.searchSubscribe){
      this.searchSubscribe.unsubscribe();
    }
  }
}
