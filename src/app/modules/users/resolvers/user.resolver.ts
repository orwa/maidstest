import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { UserApiService } from '../service/userApi.service';

@Injectable()
export class UserResolverService implements Resolve<any> {
  constructor(private userApiService: UserApiService) {}

  resolve(route: ActivatedRouteSnapshot): Observable<any> | Promise<any> | any {
    return new Promise<any>(async (resolve, reject) => {
      const userId = route.paramMap.get("id");
      this.userApiService.fetchUser(userId).subscribe(
        (result) => {
          return resolve(result);
        },
        (error) => {
          return resolve([]);
        }
      );
    });
  }
}
