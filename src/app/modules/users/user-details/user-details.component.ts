import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {Location} from '@angular/common';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.sass']
})
export class UserDetailsComponent implements OnInit {
   user:any ;
   isLoading = false;
  constructor(private location: Location,private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.user =  this.route.snapshot.data.user.data;
    console.log(this.user);

  }
  backClicked() {
    this.isLoading = true ;
    this.location.back();
  }

}
